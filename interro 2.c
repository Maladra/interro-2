#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int chiffreJoueur[4]; //Permet d'enregistrer les chiffres du joueur
struct Mastermind
{
    int combinaisonADeviner[4];
    int essaisRestantsJoueur;
    int NbPartiesGagnees;
    int NbPartiesPerdues;
};

int comparaison(struct Mastermind jeu) //Fonction pour le test de victoire mais innachev�
{
    int nombreBienPlace=0; //Compte le nombre de chiffre bien plac�
    int nombreMalPlace=0; //Compte le nombre de chiffre mal plac�
    int i;
    int y;
    for (i=0; i<4; i++)
    {

        if(jeu.combinaisonADeviner[i] == chiffreJoueur[i]) //Test si un nombre est bien plac�
        {
            nombreBienPlace++;

            jeu.essaisRestantsJoueur--;
            printf("\nIl y'a %d nombre bien place\n",nombreBienPlace);

        }

        for (y=0; y<4; y++)
        {

            if (chiffreJoueur[i]==jeu.combinaisonADeviner[y])
            {
                nombreMalPlace++;
                printf("\nIl y'a %d nombre mal place\n",nombreMalPlace);
                jeu.essaisRestantsJoueur--;
            }
        }
        if (nombreBienPlace==4) //Fait gagner la partie
        {
            printf("\nLa partie est gagnee\n");
        }
    }

    return 0;
}

int main()
{
    srand(time(NULL)); //Permet d'initialiser le random

    int rejouer=0; //Permet de rejouer

    struct Mastermind jeu;
    jeu.essaisRestantsJoueur;
    jeu.NbPartiesGagnees=0;
    jeu.NbPartiesPerdues=0;

    do
    {
        jeu.essaisRestantsJoueur=10;

        int i;
        for (i=0; i<4; i++) //Permet de placer de mani�re al�atoire les nombres pour le jeu
        {
            jeu.combinaisonADeviner[i]=rand()%10;
        }

        while(jeu.essaisRestantsJoueur>0) //Permet de jouer tant que nous avons encore des essais.
        {
            printf("Vous avez : %d essais\n",jeu.essaisRestantsJoueur);

            //Permet a l'utilisateur de donner sa combinaison
            printf("Votre premier chiffre de la combinaison : ");
            scanf("%d", &chiffreJoueur[0]);
            printf("Votre deuxieme chiffre de la combinaison : ");
            scanf("%d", &chiffreJoueur[1]);
            printf("Votre troisieme chiffre de la combinaison : ");
            scanf("%d", &chiffreJoueur[2]);
            printf("Votre quatrieme chiffre de la combinaison : ");
            scanf("%d", &chiffreJoueur[3]);

            /* comparaison(jeu);
            L'appel a la fonction qui est innachev�e */


            //Permet de continuer le programme sans fonction de tester la victoire ainsi que la defaite
            if(chiffreJoueur[0]==jeu.combinaisonADeviner[0]&&chiffreJoueur[1]==jeu.combinaisonADeviner[1]&&chiffreJoueur[2]==jeu.combinaisonADeviner[2]&&chiffreJoueur[3]==jeu.combinaisonADeviner[3])
            {
                printf("La combinaison est bonne vous gagnez\n");
                jeu.NbPartiesGagnees++; //Permet d'incrementer le nombre de victore
                break;
            }
            else
            {
                printf("\nLa combinaison n'est pas la bonne\n\n");
                jeu.essaisRestantsJoueur--; //Permet de decrementer le nombre d'essai apr�s chaque erreur
            }
            if (jeu.essaisRestantsJoueur<1) //Permet de tester si la partie est perdue
            {
                printf("Vous avez perdu vous n'avez plus d'essai\n");
                jeu.NbPartiesPerdues++; //Permet d'incrementer le nombre de partie perdue
            }
        }
        printf("\nVous avez gagne %d parties\n",jeu.NbPartiesGagnees);
        printf("Vous avez perdu %d parties\n",jeu.NbPartiesPerdues);

        printf("\nVoulez vous rejouer ?\n[0]:non\n[1]:oui\n");
        scanf("%d",&rejouer); //Permet de rejouer
    }
    while (rejouer==1); //Permet de rejouer si la condition est valide

    return 0;
}


